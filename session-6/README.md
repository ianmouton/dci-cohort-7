### Session 6: WordPress Custom Themes

Your theme is a collection of php, css, and javascript files all contained within a single folder.  To install your theme, you can copy it under /wp-content/themes

WordPress will tell you if something is wrong.  In the themes administration, a broken theme will be listed at the bottom of the page, along with a message describing the problem.

##### Conventions

WordPress will look for specific files, based on their names:

```
index.php
```

This is the main template file for your theme. WordPress will look for a file with this name.

```
header.php
```

This template file should contain the html used for your website header.  Typically, your header remains constant across multiple pages.  This "grounds" your site and provides familiar navigation to your visitors.

```
footer.php
```

Like the header, but at the bottom of your pages! The footer can often repeat navigation links, or even present a mini-site map.  

```
sidebar.php
```

You may notice the bundled themes have a narrow column along the right-hand side.  This is the sidebar.  You can customize the content within your sidebar, but it is separated from the content of your site.

```
style.css
```

This CSS file will define all the styles for your website. You can certainly break this out into separate files and even use SASS files which compile to a single file with this name.

##### Prototyping within WordPress

Navigating within the administration site to Appearance > Editor will present you with a text editor view of all your theme files.  You can edit them right there!  Behind the scenes, WordPress is actually updating the files within your installed theme (within the wp-content/themes folder).

##### Useful guides

* https://code.tutsplus.com/tutorials/creating-a-wordpress-theme-from-static-html-creating-template-files--wp-33939
* https://webdesign.tutsplus.com/tutorials/building-a-wordpress-theme-in-60-seconds--cms-24315

